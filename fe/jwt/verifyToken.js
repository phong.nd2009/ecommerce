const jsonWebToken = require('jsonwebtoken');
const fs = require('fs');
const appConfig = require('./../config/appConfig');

exports.isAuthenticated = function isAuthenticated(req, res, next) {
  const token = req.body.token || req.query.token || req.headers['x-access-token'];
  if (token) {
      const verifyOptions = {
       issuer: 'Mysoft corp',
       subject: 'some@user.com',
       audience: 'http://mysoftcorp.in',
       expiresIn: "12h",
       algorithm: "RS256"
      }
      const publicKey  = fs.readFileSync('./public.key', 'utf8');
      jsonWebToken.verify(token, publicKey, verifyOptions, (err, decoded) => {
      if (err) {
        return res.json({ success: false, message: 'Failed to authenticate token.' });
      }
      req.decoded = decoded;
      return next();
    });
  } else {
    return res.status(403).send({
      success: false,
      message: 'No token provided.',
    });
  }
};
