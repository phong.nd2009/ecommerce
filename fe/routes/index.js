const express = require('express');

const router = express.Router();
const authenRoutes = require('./v1/auth');

const userRoutes = require('./v1/user');

router.get('/', (req, res) => res.status(200).send({
  error: false,
  message: 'Welcome to the beginning of api.',
}));
router.use('/', authenRoutes);
router.use('/user', userRoutes);
module.exports = router;
