const express = require('express');

const router = express.Router();
const authenController = require('./../../controllers/authenController');
const userValidator = require('./../../validator/userValidator');

router.post(
  '/login',
  userValidator.userLoginValidator,
  authenController.handleSignIn,
);

router.post(
  '/register',
  userValidator.userRegistValidator,
  authenController.handleSignUp,
);

module.exports = router;

