const express = require('express');
const fs = require('fs')
const sharp = require('sharp');

const router = express.Router();
const userController = require('./../../controllers/userController');
const tokenService = require('./../../jwt/verifyToken');
const userValidator = require('./../../validator/userValidator');
const upload = require('./../../helpers/uploadImage');

const singleUpload = upload.single('profile');

router.get(
  '/all',
  tokenService.isAuthenticated,
  userController.allUser,
);

router.get(
    '/:id',
    tokenService.isAuthenticated,
    userController.singleUser,
  );

  router.put(
    '/:id',
    tokenService.isAuthenticated,
    userValidator.userUpdateValidator,
    userController.updateUser,
);
  
router.delete(
    '/:id',
    tokenService.isAuthenticated,
    userController.removeUser,
);

router.post('/upload_image', singleUpload, function(req, res, next) {
  if (!req.body && !req.files) {
      res.json({ success: false });
  } else {
       res.json({ success: true, file: req.file }); 
       const path = './uploads/';
       sharp(req.file.path).resize(262, 262).toFile(path + '262x262-'+req.file.filename, function(err) {
          if (err) {
              console.error('sharp>>>', err)
          }
          console.log('ok okoko')
      })

  }
});

module.exports = router;

