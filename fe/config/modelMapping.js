const Sequelize = require('sequelize');
const db = require('../models/sequelize');
const UserModel = require('../models/user');
const AccommodationModel = require('../models/accommodation');

const User = UserModel(db.sequelize, Sequelize);
const Accommodation = AccommodationModel(db.sequelize, Sequelize);

module.exports = User;