const User = require('../config/modelMapping');

exports.allUser = function allUser(req, res) {
  const order = [['id', 'ASC']];
  const limit = req.query.limit || 10;
  const offset = req.query.offset || 0;
  User
    .findAll({ order, limit, offset })
    .then(users => res.status(200).json({ error: false, data: users }))
    .catch(err => res.status(500).json({ error: true, message: err.message }));
};

exports.singleUser = function singleUser(req, res) {
  User.findByPk(req.params.id)
    .then((user) => {
      if (!user) {
        return res.status(400).send({ error: 'No user found' });
      }
      return res.status(200).json({ error: false, data: user });
    })
    .catch(err => res.status(500).send({ error: true, message: err.message }));
};


exports.removeUser = function removeUser(req, res) {
  User.findByPk(req.params.id)
    .then((user) => {
      if (!user) {
        return res.status(404).send({
          message: 'User Not Found',
        });
      }

      return user
        .destroy()
        .then(() => res.status(204).json({ error: false, data: 'Deleted' }))
        .catch(err => res.status(500).json({ error: true, message: err.message }));
    })
    .catch(err => res.status(500).json({ error: true, message: err.message }));
};


exports.updateUser = function updateUser(req, res) {
  const data = req.body;
  User.update(
    data,
    { returning: true, where: { id: req.params.id } },
  ).then(([rowsUpdate, [updatedUser]]) => {
    if (!rowsUpdate) {
      return res.status(404).send({
        message: 'User Not Found',
      });
    }
    return updatedUser;
  }).then(users => res.status(200).json({ error: false, data: users }))
    .catch(err => res.status(500).json({ error: true, message: err.message }));
};