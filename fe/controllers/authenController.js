const bcrypt = require('bcrypt');
const fs = require('fs');
const jsonWebToken = require('jsonwebtoken');
const uuidv4 = require('uuid/v4')

const User = require('../config/modelMapping');

module.exports = {
  handleSignUp: (req, res) => {
    const username = req.body.username.trim();
    const secret = req.body.secret.trim();
    // Check if username already exists
    User.findAll({
      where: { username },
    })
      .then((user) => {
        if (user.length > 0) {
          return res
            .status(400)
            .send({ error: true, message: 'The username is already registered.' });
        }

        const salt = bcrypt.genSaltSync(10);
        const hash = bcrypt.hashSync(secret, salt);
        const uuid = uuidv4();
        console.log("_uuid", uuid)
        const newUser = {
          username,
          secret: hash,
          userid: uuid,
          fullname: req.body.fullname, 
          host: req.body.host,
          role: req.body.role,
          createdat: req.body.createdat.trim(),
          updatedat: req.body.updatedat.trim()
        };

        return User.create(newUser)
          .then(() => res.status(201).json({ error: false, data: 'OK' }))
          .catch(err => res.status(500).send({ error: err.message }));
      })
      .catch(err => res.status(500).json({ error: true, message: err.message }));
  },
  handleSignIn: (req, res) => {
    const { username } = req.body;
    const { secret } = req.body;

    User.findAll({
      where: {
        username,
      },
    })
      .then((user) => {
        if (user.length === 0) {
          return res.status(401).json({
            error: true,
            message: 'No user with the given username',
          });
        }
        // checking the secret
        if (!bcrypt.compareSync(secret, user[0].secret)) {
          return res.status(401).send({ error: true, message: 'Incorrect credentials' });
        }
        const payload = {
          username: user[0].username,
        };
        const signOptions = {
          issuer: 'Mysoft corp',
          subject: 'some@user.com',
          audience: 'http://mysoftcorp.in',
          expiresIn: "12h",
          algorithm: "RS256"
         }
        const privateKey  = fs.readFileSync('./private.key', 'utf8');
        const accessToken = jsonWebToken.sign(payload, privateKey, signOptions);

        return res.status(200).json({ error: false, data: { token: accessToken } });
      })
      .catch(err => res.status(500).json({ error: true, message: err.message }));
  },
  
};

exports.logOut = function (req, res, next) {

};

